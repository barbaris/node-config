const env = process.env.NODE_ENV || 'dev';
const configDir = require('path').dirname(require.main.filename) + '/config/';
const fs = require('fs');
const config = require(configDir + 'config.json');
const configEnv = fs.existsSync(configDir + 'config-' + env + '.json') ? require(configDir + 'config-' + env + '.json') : {};
const parameters = fs.existsSync(configDir + 'parameters.json') ? require(configDir + 'parameters.json') : {};
const verbosity = require('barbaris-verbosity');

mergeObjects(config, configEnv, parameters);
if (verbosity >= 2) {
    console.log('Used config:\n', JSON.stringify(config, null, 2));
}
let result = {};

for (let key in config) {
    if (config.hasOwnProperty(key)) {
        result[key + 'Config'] = config[key];
    }
}

module.exports = result;

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
function isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
}

/**
 * Deep merge two objects.
 * @param target
 * @param sources
 */
function mergeObjects(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (isObject(target) && isObject(source)) {
        for (const key in source) {
            if (source.hasOwnProperty(key)) {
                if (isObject(source[key])) {
                    if (!target[key]) Object.assign(target, {[key]: {}});
                    mergeObjects(target[key], source[key]);
                } else {
                    Object.assign(target, {[key]: source[key]});
                }
            }
        }
    }

    return mergeObjects(target, ...sources);
}
